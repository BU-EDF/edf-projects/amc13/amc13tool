LIBRARY_EXCEPTION_SOURCES  = 
LIBRARY_EXCEPTION_OBJECT_FILES = 

PLUGINS= $(wildcard plugins/*)
SYM_LINKS=$(patsubst %,BUTool/%,${PLUGINS})

FLAGS = $(ifeq $(MAKEFLAGS) "","",-$(MAKEFLAGS))


export CACTUS_ROOT
#FLAGS += CACTUS_ROOT=$(CACTUS_ROOT)
#FLAGS += UIO_UHAL_PATH=$(UIO_UHAL_PATH)

all: local

local: ${SYM_LINKS}
	$(MAKE) ${FLAGS} -C BUTool -f mk/Makefile.local

init: 
	git submodule update --init --recursive

BUTool/%:%
	@ln -s ../../$< $@

links: ${SYM_LINKS}


install: 
	$(MAKE) install ${FLAGS} -C BUTool -f mk/Makefile.local

clean:
	@$(MAKE) ${FLAGS} -C BUTool -f mk/Makefile.crosscompile clean
	@rm -rf ${SYM_LINKS}
